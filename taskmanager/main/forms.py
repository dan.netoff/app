from .models import Task
from django.forms import ModelForm, TextInput, Textarea


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ['title', 'task']
        widgets = {
            "title": TextInput(attrs={
                'placeholder': "add title",
                'class': "form-control"
            }),
            "task": Textarea(attrs={
                'placeholder': "add task",
                'class': "form-control",
                'rows': '3'
            }),

        }
